Event Generator

Abstract
An application which generates a stream of events and writes them into a file.

To start application use maven: mvn tomcat7:run

Parameters for application located at config.properties file at src\main\webapp\WEB-INF.
Output file is log.txt at kernel directory

