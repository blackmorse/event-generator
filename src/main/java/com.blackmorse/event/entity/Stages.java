package com.blackmorse.event.entity;

/**
 * Created by black on 22.07.2017.
 */
public enum Stages implements NamedEnum{
    START("start"),
    JOIN("join"),
    END("stop");

    private String name;

    private Stages(String name){
        this.name = name;
    }

    @Override
    public String getName(){
        return name;
    }
}
