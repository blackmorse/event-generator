package com.blackmorse.event.entity;

/**
 * Created by black on 22.07.2017.
 */
public enum OriginationPage implements NamedEnum{
    LOGIN("login"),
    BALANCE("balance"),
    TRANSFER("transfer");

    private String name;

    private OriginationPage(String name){
        this.name = name;
    }

    @Override
    public String getName(){
        return name;
    }
}
