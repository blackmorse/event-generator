package com.blackmorse.event.entity;

/**
 * Created by black on 23.07.2017.
 */
public interface NamedEnum {
    String getName();
}
