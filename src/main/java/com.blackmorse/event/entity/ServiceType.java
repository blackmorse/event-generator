package com.blackmorse.event.entity;

/**
 * Created by black on 22.07.2017.
 */
public enum ServiceType implements NamedEnum{
    NEW_ACCOUNT("new account"),
    PAYMENT("payment"),
    DELIVERY("delivery");

    private String name;

    private ServiceType(String name){
        this.name = name;
    }

    @Override
    public String getName(){
        return name;
    }
}
