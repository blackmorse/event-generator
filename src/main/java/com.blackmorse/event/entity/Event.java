package com.blackmorse.event.entity;

import java.sql.Timestamp;

/**
 * Created by black on 22.07.2017.
 */
public class Event {
    private Long id;

    private Stages stage = Stages.START;

    private Timestamp eventTimeStamp;

    private Timestamp createTime;

    private OriginationPage originationPage;

    private ServiceType serviceType;

    private Timestamp deliveryTime;

    private String agentId;

    private EndReason endReason;

    private Timestamp endTime;

    private OriginationChannel originationChannel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Stages getStage() {
        return stage;
    }

    public void setStage(Stages stage) {
        this.stage = stage;
    }

    public Timestamp getEventTimeStamp() {
        return eventTimeStamp;
    }

    public void setEventTimeStamp(Timestamp eventTimeStamp) {
        this.eventTimeStamp = eventTimeStamp;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public OriginationPage getOriginationPage() {
        return originationPage;
    }

    public void setOriginationPage(OriginationPage originationPage) {
        this.originationPage = originationPage;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public Timestamp getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Timestamp deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public EndReason getEndReason() {
        return endReason;
    }

    public void setEndReason(EndReason endReason) {
        this.endReason = endReason;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", stage=" + stage +
                ", eventTimeStamp=" + eventTimeStamp +
                ", createTime=" + createTime +
                ", originationPage=" + originationPage +
                ", serviceType=" + serviceType +
                ", deliveryTime=" + deliveryTime +
                ", agentId='" + agentId + '\'' +
                ", endReason=" + endReason +
                ", endTime=" + endTime +
                '}';
    }

    public OriginationChannel getOriginationChannel() {
        return originationChannel;
    }

    public void setOriginationChannel(OriginationChannel originationChannel) {
        this.originationChannel = originationChannel;
    }
}
