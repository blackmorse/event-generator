package com.blackmorse.event.entity;

/**
 * Created by black on 23.07.2017.
 */
public enum OriginationChannel implements NamedEnum{
    WEBCHAT("webchat"),
    SMS("sms"),
    WECHAT("wechat");

    private String name;

    private OriginationChannel(String name){
        this.name = name;
    }

    @Override
    public String getName(){
        return name;
    }
}
