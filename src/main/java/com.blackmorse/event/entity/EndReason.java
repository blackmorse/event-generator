package com.blackmorse.event.entity;

/**
 * Created by black on 22.07.2017.
 */
public enum EndReason implements NamedEnum{
    NORMAL("Normal"),
    ABNORMAL("Abnormal");

    private String name;

    private EndReason(String name){
        this.name = name;
    }

    @Override
    public String getName(){
        return name;
    }
}
