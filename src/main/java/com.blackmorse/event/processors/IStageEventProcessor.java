package com.blackmorse.event.processors;

import com.blackmorse.event.entity.Event;

/**
 * Created by black on 23.07.2017.
 */
public interface IStageEventProcessor{
    public void process(Event event);

    public void postProcess(Event event);

    public Integer getDelay();
}
