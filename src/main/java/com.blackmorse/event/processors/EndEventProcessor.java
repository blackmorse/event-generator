package com.blackmorse.event.processors;

import com.blackmorse.event.common.EventUtils;
import com.blackmorse.event.entity.Event;
import org.springframework.stereotype.Component;

/**
 * Created by black on 22.07.2017.
 */
@Component
public class EndEventProcessor extends StageEventProcessor{
    private static final Integer MINIMUM_DELAY = 15;

    private static final Integer MAXIMUM_DELAY = 60;

    public EndEventProcessor() {
        super(MINIMUM_DELAY, MAXIMUM_DELAY);
    }

    @Override
    public void process(Event event) {
        EventUtils.fillEventAtEndStage(event);
    }

    @Override
    public void postProcess(Event event) {
        event.setStage(null);
    }
}
