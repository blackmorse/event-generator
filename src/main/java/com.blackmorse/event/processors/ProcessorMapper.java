package com.blackmorse.event.processors;

import com.blackmorse.event.entity.Stages;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * Created by black on 22.07.2017.
 */
@Component
public class ProcessorMapper {
    private static final Map<Stages, IStageEventProcessor> map = Maps.newHashMap();

    @Autowired
    @Qualifier("startEventProcessor")
    private IStageEventProcessor startEventProcessor;

    @Autowired
    @Qualifier("joinEventProcessor")
    private IStageEventProcessor joinEventProcessor;

    @Autowired
    @Qualifier("endEventProcessor")
    private IStageEventProcessor endEventProcessor;

    @PostConstruct
    public void init(){
        map.put(Stages.START, startEventProcessor);
        map.put(Stages.JOIN, joinEventProcessor);
        map.put(Stages.END, endEventProcessor);
    }

    public IStageEventProcessor getProcessor(Stages stage){
        return map.get(stage);
    }
}
