package com.blackmorse.event.processors;

import com.blackmorse.event.common.EventUtils;
import com.blackmorse.event.entity.Event;
import com.blackmorse.event.entity.Stages;
import org.springframework.stereotype.Component;

/**
 * Created by black on 22.07.2017.
 */
@Component
public class JoinEventProcessor extends StageEventProcessor{
    private static final Integer MINIMUM_DELAY = 3;

    private static final Integer MAXIMUM_DELAY = 10;

    public JoinEventProcessor() {
        super(MINIMUM_DELAY, MAXIMUM_DELAY);
    }

    @Override
    public void process(Event event) {
        EventUtils.fillEventAtJoinStage(event);
    }

    @Override
    public void postProcess(Event event) {
        event.setStage(Stages.END);
    }
}
