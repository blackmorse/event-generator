package com.blackmorse.event.processors;

import com.blackmorse.event.common.EventUtils;
import com.blackmorse.event.entity.Event;
import com.blackmorse.event.entity.Stages;
import org.springframework.stereotype.Component;

/**
 * Created by black on 22.07.2017.
 */
@Component
public class StartEventProcessor extends StageEventProcessor{

    public StartEventProcessor() {
        super(0, 0);
    }

    @Override
    public void process(Event event) {
        EventUtils.fillEventAtStartStage(event);
    }

    @Override
    public void postProcess(Event event) {
        event.setStage(Stages.JOIN);
    }
}
