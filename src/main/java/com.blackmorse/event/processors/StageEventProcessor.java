package com.blackmorse.event.processors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

/**
 * Created by black on 22.07.2017.
 */
public abstract class StageEventProcessor implements IStageEventProcessor{
    private static final Logger LOGGER = LoggerFactory.getLogger(StageEventProcessor.class);

    private final Integer minDelay, maxDelay;

    private final Random random = new Random();

    public StageEventProcessor(Integer minDelay, Integer  maxDelay){
        this.minDelay = minDelay;
        this.maxDelay = maxDelay;
    }

    @Override
    public Integer getDelay(){
        if (maxDelay - minDelay <= 0){
            return 0;
        }
        return minDelay + random.nextInt(maxDelay - minDelay);
    }
}
