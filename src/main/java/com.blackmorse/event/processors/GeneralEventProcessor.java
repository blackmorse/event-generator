package com.blackmorse.event.processors;

import com.blackmorse.event.IStorer;
import com.blackmorse.event.entity.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;
import rx.Observable;
import rx.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;


/**
 * Created by black on 22.07.2017.
 */
@Component
public class GeneralEventProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralEventProcessor.class);

    @Autowired
    private ProcessorMapper processorMapper;

    @Autowired
    private IStorer eventStorer;

    @Autowired
    @Qualifier("poolTaskExecutor")
    private TaskExecutor poolTaskExecutor;

    @Autowired
    @Qualifier("singleTaskExecutor")
    private TaskExecutor singleTaskExecutor;

    public void processEvent(Event currentEvent) {
        IStageEventProcessor stageEventProcessor = processorMapper.getProcessor(currentEvent.getStage());
        final Integer delay = stageEventProcessor.getDelay();
        Observable.just(currentEvent).doOnNext(this::process)
                .subscribeOn(Schedulers.from(poolTaskExecutor))
                .delaySubscription(delay, TimeUnit.SECONDS)
                .observeOn(Schedulers.from(singleTaskExecutor))
                .subscribe(this::subscribeEvent, this::onError);
    }

    private void process(Event event){
        processorMapper.getProcessor(event.getStage()).process(event);
    }

    private void subscribeEvent(Event event){
        eventStorer.doStore(event);
        processorMapper.getProcessor(event.getStage()).postProcess(event);
        if (event.getStage() != null) {
            processEvent(event);
        }
    }

    private void onError(Throwable e){
        LOGGER.error(e.getMessage(), e);
    }
}
