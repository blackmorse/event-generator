package com.blackmorse.event;

import com.blackmorse.event.common.JsonUtils;
import com.blackmorse.event.entity.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Created by black on 22.07.2017.
 */
@Component
public class EventStorer implements IStorer{
    private static final Logger LOGGER = LoggerFactory.getLogger(EventStorer.class);

    private static final String FILE_NAME = "log.txt";

    @Autowired
    private EventGenerator eventGenerator;

    @Override
    public void doStore(Event event) {
        LOGGER.info(event.toString());
        try(PrintWriter output = new PrintWriter(new FileWriter(FILE_NAME,true)))
        {
            output.printf("%s\r\n", JsonUtils.convertEvent(event));
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
