package com.blackmorse.event;

import com.blackmorse.event.entity.Event;

/**
 * Created by black on 23.07.2017.
 */
public interface IStorer {
    public void doStore(Event event);
}
