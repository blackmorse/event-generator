package com.blackmorse.event.common;

import com.blackmorse.event.entity.*;

import java.sql.Timestamp;
import java.util.Random;

/**
 * Created by black on 22.07.2017.
 */
public final class EventUtils {
    private static final String AGENT = "Agent_";

    private static final Integer AGENT_NUMBERS = 1000;

    private static final Random RANDOM = new Random();

    private EventUtils(){}

    public static Event createEvent(IGenerator<Long> idGenerator){
        Event event = new Event();
        event.setId(idGenerator.getId());

        return event;
    }

    public static void fillEventAtStartStage(Event event){
        event.setStage(Stages.START);
        Timestamp time = new Timestamp(System.currentTimeMillis());
        event.setCreateTime(time);
        event.setEventTimeStamp(time);
        event.setOriginationPage(generateEnum(OriginationPage.values()));
        event.setServiceType(generateEnum(ServiceType.values()));
        event.setOriginationChannel(generateEnum(OriginationChannel.values()));
    }

    public static void fillEventAtJoinStage(Event event){
        Timestamp time = new Timestamp(System.currentTimeMillis());
        event.setDeliveryTime(time);
        event.setEventTimeStamp(time);
        event.setAgentId(AGENT + RANDOM.nextInt(AGENT_NUMBERS));
    }

    public static void fillEventAtEndStage(Event event){
        Timestamp time = new Timestamp(System.currentTimeMillis());
        event.setEndReason(generateEnum(EndReason.values()));
        event.setEndTime(time);
        event.setEventTimeStamp(time);
    }

    private static <E extends Enum<E>> E generateEnum(E[] values){
        Random random = new Random();
        int n = random.nextInt(values.length);
        return values[n];
    }
}
