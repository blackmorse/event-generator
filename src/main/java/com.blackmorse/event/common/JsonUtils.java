package com.blackmorse.event.common;

import com.blackmorse.event.entity.Event;
import com.blackmorse.event.entity.NamedEnum;
import org.json.simple.JSONObject;

import java.sql.Timestamp;

/**
 * Created by black on 22.07.2017.
 */
public final class JsonUtils {
    private static final String UNDEFINED = "undefined";

    private JsonUtils(){};

    public static JSONObject convertEvent(Event event){
        Timestamp minimumTime = getMinimumTime(event.getCreateTime(), event.getDeliveryTime(), event.getEventTimeStamp(), event.getEndTime());

        JSONObject object = new JSONObject();
        object.put("id", event.getId());
        object.put("EventType", getEnumString(event.getStage()));
        object.put("EventTimeStamp", getJsonDate(event.getEventTimeStamp(), minimumTime));
        object.put("CreateTime", getJsonDate(event.getCreateTime(), minimumTime));
        object.put("DeliveryTime", getJsonDate(event.getDeliveryTime(), minimumTime));
        object.put("EndTime", getJsonDate(event.getEndTime(), minimumTime));
        object.put("ServiceType", getEnumString(event.getServiceType()));
        object.put("OriginationPage", getEnumString(event.getOriginationPage()));
        object.put("AgentId", getJsonString(event.getAgentId()));
        object.put("EndReason", getEnumString(event.getEndReason()));
        object.put("OriginationChannel", getEnumString(event.getOriginationChannel()));

        return object;
    }

    private static Object getJsonString(Object object){
         return object != null ? object : UNDEFINED;
    }

    private static Timestamp getMinimumTime(Timestamp ... times){
        Timestamp result = null;
        for (Timestamp time : times){
            if (time != null && (result == null || time.before(result))){
                result = time;
            }
        }
        return result;
    }

    private static String getEnumString(NamedEnum en){
        return en != null ? en.getName() : UNDEFINED;
    }

    private static String getJsonDate(Timestamp time, Timestamp minimumTime){
        if (time != null){
            return time.toString();
        } else if (minimumTime != null){
            return minimumTime.toString();
        } else {
            return UNDEFINED;
        }
    }
}
