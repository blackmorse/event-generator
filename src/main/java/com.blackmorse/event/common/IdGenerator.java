package com.blackmorse.event.common;

import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by black on 22.07.2017.
 */
@Component
public class IdGenerator implements IGenerator<Long>{
    private final AtomicLong currentId = new AtomicLong(0L);

    @Override
    public Long getId(){
        return currentId.incrementAndGet();
    }
}
