package com.blackmorse.event.common;

/**
 * Created by black on 23.07.2017.
 */
public interface IGenerator<T> {
    public T getId();
}
