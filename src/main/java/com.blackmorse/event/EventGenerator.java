package com.blackmorse.event;

import com.blackmorse.event.common.EventUtils;
import com.blackmorse.event.common.IGenerator;
import com.blackmorse.event.entity.Event;
import com.blackmorse.event.processors.GeneralEventProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * Created by black on 22.07.2017.
 */
@Component
public class EventGenerator{
    private static final Logger LOGGER = LoggerFactory.getLogger(EventGenerator.class);

    @Autowired
    private GeneralEventProcessor generalEventProcessor;

    @Autowired
    private IGenerator<Long> idGenerator;

    @Value("${rate}")
    private Integer rate;

    @Value("${number}")
    private Integer number;

    @PostConstruct
    public void generate(){
        int delay = 1000 / rate;
        LOGGER.info("Start generating {} events with rate {} per second", number, rate);
        for (int i = 0; i < number; i++) {
            Event event = EventUtils.createEvent(idGenerator);
            generalEventProcessor.processEvent(event);
            sleep(delay);
        }
        LOGGER.info("Generation success");
    }

    private void sleep(long time){
        try {
            TimeUnit.MILLISECONDS.sleep(time);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
