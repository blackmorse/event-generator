package com.blackmorse.event.common;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by black on 23.07.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class IdGeneratorTest {
    @Test
    public void getId() throws Exception {
        IGenerator<Long> idGenerator = new IdGenerator();
        Assert.assertTrue(idGenerator.getId().equals(1L));
        Assert.assertTrue(idGenerator.getId().equals(2L));
        Assert.assertTrue(idGenerator.getId().equals(3L));
        Assert.assertTrue(idGenerator.getId().equals(4L));
    }

}