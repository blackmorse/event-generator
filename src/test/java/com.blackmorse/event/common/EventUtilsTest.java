package com.blackmorse.event.common;

import com.blackmorse.event.entity.Event;
import com.blackmorse.event.entity.Stages;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by black on 23.07.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class EventUtilsTest {

    @Mock
    private IdGenerator idGenerator;

    @Test
    public void createEvent() throws Exception {
        Event event = EventUtils.createEvent(idGenerator);
        assertNotNull(event);
        Mockito.verify(idGenerator).getId();
    }

    @Test
    public void fillEventAtStartStage() throws Exception {
        Event event = new Event();
        EventUtils.fillEventAtStartStage(event);

        assertEquals(event.getStage(), Stages.START);
        assertNotNull(event.getCreateTime());
        assertNotNull(event.getEventTimeStamp());
        assertEquals(event.getCreateTime(), event.getEventTimeStamp());
        assertNotNull(event.getOriginationPage());
        assertNotNull(event.getOriginationChannel());
        assertNotNull(event.getServiceType());
    }

    @Test
    public void fillEventAtJoinStage() throws Exception {
        Event event = new Event();
        EventUtils.fillEventAtJoinStage(event);

        assertNotNull(event.getDeliveryTime());
        assertNotNull(event.getEventTimeStamp());
        assertEquals(event.getDeliveryTime(), event.getEventTimeStamp());
        assertNotNull(event.getAgentId());
        assertTrue(event.getAgentId().contains("Agent_"));
    }

    @Test
    public void fillEventAtEndStage() throws Exception {
        Event event = new Event();
        EventUtils.fillEventAtEndStage(event);

        assertNotNull(event.getEndReason());
        assertNotNull(event.getEventTimeStamp());
        assertNotNull(event.getEndTime());
        assertEquals(event.getEventTimeStamp(), event.getEndTime());
    }


}
