package com.blackmorse.event;

import com.blackmorse.event.common.IGenerator;
import com.blackmorse.event.processors.GeneralEventProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by black on 23.07.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class EventGeneratorTest {

    @InjectMocks
    private EventGenerator eventGenerator = new EventGenerator();

    @Mock
    private GeneralEventProcessor generalEventProcessor;

    @Mock
    private IGenerator<Long> idGenerator;

    @Test
    public void generate() throws Exception {
        Whitebox.setInternalState(eventGenerator, "rate", Integer.MAX_VALUE);
        Whitebox.setInternalState(eventGenerator, "number", 1);
        when(idGenerator.getId()).thenReturn(1L);

        eventGenerator.generate();

        verify(generalEventProcessor).processEvent(any());
    }
}