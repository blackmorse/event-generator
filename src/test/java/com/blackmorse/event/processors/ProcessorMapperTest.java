package com.blackmorse.event.processors;

import com.blackmorse.event.entity.Stages;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by black on 23.07.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProcessorMapperTest {

    @InjectMocks
    private ProcessorMapper processorMapper = new ProcessorMapper();

    @Mock(name = "startEventProcessor")
    private StartEventProcessor startEventProcessor;

    @Mock(name = "joinEventProcessor")
    private JoinEventProcessor joinEventProcessor;

    @Mock(name = "endEventProcessor")
    private EndEventProcessor endEventProcessor;

    @Test
    public void getProcessor(){
        processorMapper.init();

        assertEquals(processorMapper.getProcessor(Stages.START), startEventProcessor);
        assertEquals(processorMapper.getProcessor(Stages.JOIN), joinEventProcessor);
        assertEquals(processorMapper.getProcessor(Stages.END), endEventProcessor);
    }

}